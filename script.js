function ValidateForm(testform){
        
    let firstname = document.forms["testform"]["firstname"].value;
    if (firstname == "") {
      alert("First Name must be filled out");
      return false
    }
        
    let lastname = document.forms["testform"]["lastname"].value;
    if (lastname == "") {
      alert("Last Name must be filled out");
      return false;
    }
        
    var email=document.testform.email.value;  
    var atposition=email.indexOf("@");  
    var dotposition=email.lastIndexOf(".");  
    if (atposition<1 || dotposition<atposition+2 || dotposition+2>=email.length){  
      alert("Please enter a valid e-mail address \n atpostion:"+atposition+"\n dotposition:"+dotposition);  
      return false;  
    } 
       
    var age = document.getElementById("age").value;
      if (age < 1 || age > 100){
      alert("enter age between 1 to 100")
      return false;
    }
       
    var gender = document.getElementsByName("gender");
    var gendervalid = false;
    var i = 0;
    while (!gendervalid && i < gender.length) {
      if (gender[i].checked) gendervalid = true;
      i++;        
    }
    if (!gendervalid){
      alert("Please select gender!");
      return false;
    }

    if((document.testform.drink[0].checked==false) && (document.testform.drink[1].checked==false) && (document.testform.drink[2])){
      alert("Select your preferences:");
      return false;
    }
        
    if (document.testform.state.selectedIndex == 0){
        alert("Please choose a  state:");
        return false;
    }
}    


    